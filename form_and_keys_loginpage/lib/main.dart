import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget{
  const LoginPage({super.key});

  @override
  State<LoginPage>createState()=> _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{
  //  CONTROLLERS
  TextEditingController userNameTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();

  // KEYS
  // GlobalKey<FormFieldState> userNameKey = GlobalKey<FormFieldState>();
  // GlobalKey<FormFieldState> passwordKey = GlobalKey<FormFieldState>();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  
  get validator => null;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Login Page"),
        //centerTitle: bool,
      ),

      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 20,
              ),
              Image.network(
                "https://w7.pngwing.com/pngs/771/79/png-transparent-avatar-bootstrapcdn-graphic-designer-angularjs-avatar-child-face-heroes-thumbnail.png",
                width: 90,
                height: 90,
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller:userNameTextEditingController,
                //Key:userNameKey,
                decoration: InputDecoration(
                  hintText: "Enter userName",
                  label: const Text("Enter userName"),
                  border: OutlineInputBorder(
                  borderRadius:BorderRadius.circular(20),
                  ),
                ),
                prefixIcon: const Icon(Icons.person),
              ),
              validator:(value){
                print("In USERNAME VALIDATOR");
                if(value == null || value.isEmpty){
                  return "Please enter username";
                }else{
                  return null;
                }
              },
              keyboardType:TextInputType.emailAddress,
            
            const SizedBox(
              height: 20,
            ),

              TextFormField(
                controller:passwordTextEditingController,
                //Key:passwordKey,
                obscureText:true,
                obscuringCharacter:"*",
                decoration:InputDecoration(
                  hintText:"Enter password",
                  border:OutlineInputBorder(
                    borderRadius:BorderRadius.circular(20),
                  ),
                  prefixIcon:const Icon(
                    Icons.lock,
                  ),
                  suffixIcon:const Icon(
                    Icons.remove_red_eye_outlined,
                  ),
                ),
                validator:(value){
                  print("IN PASSWORD VALIDATION");
                  if(value == null || value.isEmpty){
                    return "Please enter password";
                  }else{
                    return null;
                  }
                }
              ),
              const SizedBox(
                height:20,
              ),
              ElevatedButton(
                onPressed: (){
                  bool loginValidated = _formKey.currentState!.validate();
                  
                  if(loginValidated){
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content:Text("Login Successful"),
                      ),
                    );
                  }else{
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content:Text("Login Failed"),
                      ),
                    );
                  }
                }, 
                child: const Text("Login"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


















